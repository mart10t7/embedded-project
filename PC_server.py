import socket
import sys
import struct

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('192.168.33.6', 10000)
print('starting up on {} port {}'.format(*server_address))
sock.bind(server_address)
fanspeed=100
fanzero = 0
# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(16)
            if data:
                num = struct.unpack('!d',data)
                temp = num[0]
                print("Raspberry CPU Temp is: ",temp)
                if temp > 50.0:
                    message = struct.pack("!d",fanspeed)
                    connection.sendall(message)
                else:
                    message = struct.pack("!d",fanzero)
                    connection.sendall(message)
            else:
                print('no data',client_address)
                break

    finally:
        # Clean up the connection
        connection.close()
